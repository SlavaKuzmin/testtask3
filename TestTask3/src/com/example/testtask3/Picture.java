package com.example.testtask3;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.example.testtask3.ImageLoader.DownloadsQueue;

public class Picture extends FragmentActivity {
	static String dir[];
	MyAdapter mAdapter;
	ViewPager mPager;
	ImageView img;
	String url;
	File Img;
	int pos;
	static public Cursor resOfPrevSch;
	
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);

	    Log.d("Picture", "�������� �����������");
	    
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
	    setContentView(R.layout.open_item);
	    
		mAdapter = new MyAdapter(getSupportFragmentManager());

		mPager = (ViewPager) findViewById(R.id.pager);
		mPager.setAdapter(mAdapter);
		
		Log.d("Picture", "w1");
	    Intent intent = getIntent();
	    resOfPrevSch=MainActivity.db.getAllData(DB.DB_TABLE_IMG+Integer.toString(ImageLoader.Index_of_search));
	    pos=intent.getIntExtra("Img", 10);
	    mPager.setCurrentItem(pos);
        resOfPrevSch.moveToPosition(pos);
        url=resOfPrevSch.getString(resOfPrevSch.getColumnIndex(DB.COLUMN_URL));
        /*if((resOfPrevSch.getString(resOfPrevSch.getColumnIndex(DB.COLUMN_IMG))==null)){
        	t.start();
        	img.setImageResource(R.drawable.ic_launcher);

        }
        else setPicture.run();*/
	  }

	public class MyAdapter extends FragmentStatePagerAdapter  {

		public MyAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public int getCount() {
			return resOfPrevSch.getCount();
		}

		@Override
		public Fragment getItem(int position) {
			return ImageFragment.init(position);
		}
	}

	public static class ImageFragment extends Fragment {
	    int fragVal;
	 
	    static ImageFragment init(int val) {
	        ImageFragment truitonFrag = new ImageFragment();
	        // Supply val input as an argument.
	        Bundle args = new Bundle();
	        args.putInt("val", val);
	        truitonFrag.setArguments(args);
	        return truitonFrag;
	    }
	 
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        fragVal = getArguments() != null ? getArguments().getInt("val") : 1;
	    }
	 
	    @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
	    	Log.d("Picture", "w2");
	        View layoutView = inflater.inflate(R.layout.img, container,
	                false);
	        Log.d("Picture", "w3");
	        View tv = layoutView.findViewById(R.id.iv);
	        resOfPrevSch=MainActivity.db.getAllData(DB.DB_TABLE_IMG+Integer.toString(ImageLoader.Index_of_search));
	        resOfPrevSch.moveToPosition(fragVal);
	        Log.d("Picture", "w4");
	        if((resOfPrevSch.getString(resOfPrevSch.getColumnIndex(DB.COLUMN_IMG))==null)){
	        	((ImageView) tv).setImageResource(R.drawable.ic_launcher);

	        }
	        else{
	        File Img= new File(resOfPrevSch.getString(resOfPrevSch.getColumnIndex(DB.COLUMN_IMG)));
	        Log.d("Picture", "w5");
	        ((ImageView) tv).setImageBitmap(decodeFile(Img));
	        Log.d("Picture", "w6");
	        }
	        return layoutView;
	    }
	}
	
	
	
	
	Runnable setPicture = new Runnable(){
		 public void run(){
		   resOfPrevSch.moveToPosition(pos);
		   File Img= new File(resOfPrevSch.getString(resOfPrevSch.getColumnIndex(DB.COLUMN_IMG)));
		   Log.d("Picture", "��������� 6");
		   img.setImageBitmap(decodeFile(Img));
		 }
	};
	
	Runnable setViewInvis = new Runnable(){
		 public void run(){
			 MainActivity.progbar.setVisibility(View.INVISIBLE);
		 }
	};
	
	Thread t = new Thread(new Runnable() {
        public void run() {
        	Log.d("Picture",""+ ImageLoader.DownloadsQueue.photosToDownload.size());
        	synchronized (DownloadsQueue.photosToDownload) {
			if(true){
        		Log.d("Picture", "���������");
        		String filename=String.valueOf(url.hashCode());
            	File f=new File(ImageLoader.cacheDir, filename);
            	Log.d("Picture", "��������� 2");
            	 try {
         	        InputStream is=new URL(url).openStream();
         	        OutputStream os = new FileOutputStream(f);
                     Utils.CopyStream(is, os);
                     os.close();
                     Log.d("Picture", "��������� 3");
		             MainActivity.db.addDir(ImageLoader.Index_of_search,url,f.getPath());
		             resOfPrevSch=MainActivity.db.getAllData(DB.DB_TABLE_IMG+Integer.toString(ImageLoader.Index_of_search));
                 }
                 catch(MalformedURLException e)
                 {
                 }
                 catch (Exception ex){
                    ex.printStackTrace();
                 }
        		
        	}
        	else{Log.d("Picture", "�����������");
        		while(ImageLoader.Down){}
        		Log.d("Picture", "���������");
        	}
        	}
        	Log.d("Picture", "��������� 4");
        	Picture.this.runOnUiThread(setPicture);
        	Activity a=(Activity)MainActivity.progbar.getContext();
        	a.runOnUiThread(setViewInvis);
        }
      });
    
    
   static public Bitmap decodeFile(File f){
    	try {
    		BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f),null,o);
			
			final int WIGHT_OF_SCREEN;			
			final int HIGHT_OF_SCREEN;
			int width_tmp=o.outWidth, height_tmp=o.outHeight;
			int scale=1;
			if(width_tmp<=height_tmp){
				WIGHT_OF_SCREEN=480;			
				HIGHT_OF_SCREEN=800;
			}
			else{
				WIGHT_OF_SCREEN=800;			
				HIGHT_OF_SCREEN=480;
			}
			while(true){
				if(width_tmp/2<WIGHT_OF_SCREEN && height_tmp/2<HIGHT_OF_SCREEN)
					break;
				width_tmp/=2;
				height_tmp/=2;
				scale++;
			}
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize=scale;
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (FileNotFoundException e) {}
		return null;
    }
	
	  void logCursor(Cursor c) {
		    if (c != null) {
		      if (c.moveToFirst()) {
		        String str;
		        do {
		          str = "";
		          for (String cn : c.getColumnNames()) {
		            str = str.concat(cn + " = " + c.getString(c.getColumnIndex(cn)) + "; ");
		          }
		          Log.d("Picture", str);
		        } while (c.moveToNext());
		      }
		    } else
		      Log.d("Picture", "Cursor is null");
		  }
}
