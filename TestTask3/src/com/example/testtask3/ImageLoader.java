package com.example.testtask3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Stack;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class ImageLoader {
	
	final String LOG_TAG = "myLogs";
	private HashMap<String, Bitmap> cache=new HashMap<String, Bitmap>();
	Cursor searches=MainActivity.searches;
	public static File cacheDir;
	Context context;
	String strSearch1=MainActivity.strSearch1;
	final int stub_id=R.drawable.ic_launcher;
	Cursor resOfPrevSch;
	public static int Index_of_search;
	public static Boolean Down;
	
	public ImageLoader(Context context,int Index_of_search, Cursor resOfPrevSch){
		this.resOfPrevSch=resOfPrevSch;
		searches.moveToPosition(Index_of_search);
		this.context = context;
		photoLoaderThread.setPriority(Thread.NORM_PRIORITY-1);
		photosDownloaderThread.setPriority(Thread.NORM_PRIORITY-2);
		//доработать
		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)){
			if(/*!(searches.isNull(searches.getColumnIndex(DB.FILE_DIR)))*/false) {
				cacheDir=new File(searches.getString(searches.getColumnIndex(DB.COLUMN_IMG)));}
				else{cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),context.getString(R.string.cache_dirname)+"/"+strSearch1);
					this.Index_of_search=Index_of_search;
				}
		}
		else{cacheDir=context.getCacheDir();
		}
		if(!cacheDir.exists()){
			cacheDir.mkdirs();
		}
	}
	
	
	public void DisplayImage(int position, Activity activity, ImageView imageView, ProgressBar progressBar)
    {	resOfPrevSch=MainActivity.db.getAllData(DB.DB_TABLE_IMG+Integer.toString(ListViewImageAdapter.Index_of_search));
		resOfPrevSch.moveToPosition(position);
	     // Log.d(LOG_TAG, ""+!(resOfPrevSch.getString(resOfPrevSch.getColumnIndex(DB.COLUMN_IMG))==null));
		if(!(resOfPrevSch.getString(resOfPrevSch.getColumnIndex(DB.COLUMN_IMG))==null)) progressBar.setVisibility(View.INVISIBLE);
    	String ur=resOfPrevSch.getString(resOfPrevSch.getColumnIndex(DB.COLUMN_URL));
    	String sur=resOfPrevSch.getString(resOfPrevSch.getColumnIndex(DB.COLUMN_SURL));
    	imageView.setTag(ur);
			if(cache.containsKey(ur)){
				//if(cache.get(ur).getHeight()>cache.get(ur).getWidth()) imageView.setScaleType(ImageView.SCALE_X);
				imageView.setImageBitmap(cache.get(ur));
				imageView.setClickable(false);
				}
			else
			{
				queuePhoto(ur, sur, activity, imageView, progressBar);
				imageView.setImageResource(stub_id);
				imageView.setClickable(true);
			}  
		
    }
        
	
    private void queuePhoto(String url, String surl, Activity activity, ImageView imageView,ProgressBar progressBar)
    {
        photosQueue.Clean(imageView);
        PhotoToLoad p=new PhotoToLoad(url, surl, imageView, progressBar);
        synchronized(photosQueue.photosToLoad){
        	photosQueue.photosToLoad.add(p);
        	photosQueue.photosToLoad.notifyAll();
        }
        if(photoLoaderThread.getState()==Thread.State.NEW)
            photoLoaderThread.start();
    }  	

    
    private class PhotoToLoad
    {	public ProgressBar progressBar;
    	public String surl;
        public String url;
        public ImageView imageView;
        public PhotoToLoad(String u, String su, ImageView i, ProgressBar l){
        	surl=su;
        	url=u; 
        	imageView=i;
        	progressBar=l;
        }
    }
    
    
    class PhotosQueue
    {
        private Stack<PhotoToLoad> photosToLoad=new Stack<PhotoToLoad>();
        
        public void Clean(ImageView image)
        {
            for(int j=0 ;j<photosToLoad.size();){
                if(photosToLoad.get(j).imageView==image)
                    photosToLoad.remove(j);
                else
                	++j;
            }
        }
    }
	
    
    PhotosQueue photosQueue=new PhotosQueue();
    
    
    public void stopThread()
    {
    	photoLoaderThread.interrupt();
    	photosDownloaderThread.interrupt();
    }
	
   
    
    
    private Bitmap getBitmap(String url,String surl, ProgressBar progressBar) 
    {
    	
    	String filename=String.valueOf(url.hashCode());
    	File f=new File(cacheDir, filename);
    	
    	Bitmap b = decodeFile(f);
    	if(b!=null)
    		return b;
    	else {
    		queueDownload(url, progressBar);
    		final ProgressBar pr=progressBar;
    		Runnable setViewInvis = new Runnable(){
    			 public void run(){
    				 pr.setVisibility(View.VISIBLE);
    			 }
    		};
    		progressBar.post(setViewInvis);
    	}
    	
        try {
	        InputStream is=new URL(surl).openStream();
	        BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new URL(surl).openStream(),null,o);
			
			final int REQUIRED_SIZE=160;
			int width_tmp=o.outWidth, height_tmp=o.outHeight;
			int scale=1;
			while(true){
				if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
					break;
				width_tmp/=2;
				height_tmp/=2;
				scale++;
			}
			
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize=scale;
			return BitmapFactory.decodeStream(new URL(surl).openStream(), null, o2);
        }
        catch(MalformedURLException e)
        {
        	Bitmap bookDefaultIcon = BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.ic_launcher);
        	return bookDefaultIcon;
        }
        catch (Exception ex){
           ex.printStackTrace();
           return null;
        }
    }

    
    
    private Bitmap decodeFile(File f){
    	try {
    		BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f),null,o);
			
			final int REQUIRED_SIZE=70;
			int width_tmp=o.outWidth, height_tmp=o.outHeight;
			int scale=1;
			while(true){
				if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
					break;
				width_tmp/=2;
				height_tmp/=2;
				scale++;
			}
			
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize=scale;
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (FileNotFoundException e) {}
		return null;
    }
	
    
    
    class PhotosLoader extends Thread {
        public void run() {
        	try {
	        	while(true)
	        	{
		        	if(photosQueue.photosToLoad.size()==0)
		        		synchronized(photosQueue.photosToLoad){
		        			photosQueue.photosToLoad.wait();
		        		}
		            if(photosQueue.photosToLoad.size()!=0)
		            {
		                PhotoToLoad photoToLoad;
		                synchronized(photosQueue.photosToLoad){
		                	photoToLoad=photosQueue.photosToLoad.pop();
		                }
		                Bitmap bmp=getBitmap(photoToLoad.url,photoToLoad.surl,photoToLoad.progressBar);
		                cache.put(photoToLoad.url, bmp);
		                if(((String)photoToLoad.imageView.getTag()).equals(photoToLoad.url)){
		                    BitmapDisplayer bd=new BitmapDisplayer(bmp, photoToLoad.imageView,photoToLoad.progressBar);
		                    Activity a=(Activity)photoToLoad.imageView.getContext();
		                    a.runOnUiThread(bd);
		                }
		            }
		            if(Thread.interrupted())
		            	break;
	        	}
        	} catch (InterruptedException e) {
 			}
        }
    }
    
    PhotosLoader photoLoaderThread=new PhotosLoader();
    
    
    
    class BitmapDisplayer implements Runnable
    {
        Bitmap bitmap;
        ImageView imageView;
        ProgressBar progressBar;
        public BitmapDisplayer(Bitmap b, ImageView i, ProgressBar l){bitmap=b;imageView=i;progressBar=l;}
        public void run()
        {
            if(bitmap!=null){
                imageView.setImageBitmap(bitmap);
            	imageView.setClickable(false);
            	}
            else
                imageView.setImageResource(stub_id);
        }
    }	
    

    void logCursor(Cursor c) {
	    if (c != null) {
	      if (c.moveToFirst()) {
	        String str;
	        do {
	          str = "";
	          for (String cn : c.getColumnNames()) {
	            str = str.concat(cn + " = " + c.getString(c.getColumnIndex(cn)) + "; ");
	          }
	          Log.d(LOG_TAG, str);
	        } while (c.moveToNext());
	      }
	    } else
	      Log.d(LOG_TAG, "Cursor is null");
	  }   
    
    
	
    public void clearCache() {
		cache.clear();
		
		File[] files=cacheDir.listFiles();
		for(File f:files)
			f.delete();
	}
    
    
    private void queueDownload(String url, ProgressBar progressBar)
    {
    	PhotoToDownload p=new PhotoToDownload(url, progressBar);
        synchronized(downloadsQueue.photosToDownload){
        	downloadsQueue.photosToDownload.add(p);
        	downloadsQueue.photosToDownload.notifyAll();
        }
        if(photosDownloaderThread.getState()==Thread.State.NEW)
        	photosDownloaderThread.start();
    }  	
    
    private class PhotoToDownload
    {	public ProgressBar progressBar;
        public String url;
        public PhotoToDownload(String u, ProgressBar b){
        	url=u; 
        	progressBar=b;
        }
    }
    
    public static class DownloadsQueue
    {
        public static Stack<PhotoToDownload> photosToDownload=new Stack<PhotoToDownload>();
        public Boolean Clean(String url)
        {
        	Boolean o=false;
            for(int j=0 ;j<photosToDownload.size();){
                if(photosToDownload.get(j).url==url){
                	photosToDownload.remove(j);
                o= true;}
                else
                	++j;
            }
            return o;
        }
    }
	
    
   public static DownloadsQueue downloadsQueue=new DownloadsQueue();
    
    class PhotosDownloader extends Thread {
        public void run() {
        	try {
	        	while(true)
	        	{
		        	if(downloadsQueue.photosToDownload.size()==0)
		        		synchronized(downloadsQueue.photosToDownload){
		        			downloadsQueue.photosToDownload.wait();
		        		}
		            if(downloadsQueue.photosToDownload.size()!=0)
		            {
		            	PhotoToDownload photoToDownload;
		                synchronized(downloadsQueue.photosToDownload){
		                	photoToDownload=downloadsQueue.photosToDownload.pop();
		                }
		                
		            	String filename=String.valueOf(photoToDownload.url.hashCode());
		            	File f=new File(cacheDir, filename);
		            	 try {
		            		 Down = true;
		         	        InputStream is=new URL(photoToDownload.url).openStream();
		         	        OutputStream os = new FileOutputStream(f);
		                     Utils.CopyStream(is, os);
		                     os.close();
				             MainActivity.db.addDir(Index_of_search,photoToDownload.url,f.getPath());
				             Log.d(LOG_TAG, "картинка скачалась");
				             Down = false;
				            // Down.notifyAll();
				             
				             final ProgressBar pr=photoToDownload.progressBar;
				     		Runnable setViewInvis = new Runnable(){
				     			 public void run(){
				     				 pr.setVisibility(View.INVISIBLE);
				     			 }
				     		};
				     		Log.d(LOG_TAG, ""+posOfUrl(resOfPrevSch,photoToDownload.url)+" "+MainActivity.firstVisibleItem+" "+(MainActivity.firstVisibleItem+MainActivity.visibleItemCount-1));
				     		if(posOfUrl(resOfPrevSch,photoToDownload.url)>=MainActivity.firstVisibleItem && posOfUrl(resOfPrevSch,photoToDownload.url)<=MainActivity.firstVisibleItem+MainActivity.visibleItemCount-1)
				     		photoToDownload.progressBar.post(setViewInvis);
				     		
		                 }
		                 catch(MalformedURLException e)
		                 {
		                 }
		                 catch (Exception ex){
		                    ex.printStackTrace();
		                 }
		            }
		            if(Thread.interrupted())
		            	break;
	        	}
        	} catch (InterruptedException e) {
 			}
        }
    }
    
    PhotosDownloader photosDownloaderThread=new PhotosDownloader();
    
    int posOfUrl(Cursor cursor, String url){
    	cursor.moveToFirst();
    	do{
    		if(cursor.getString(cursor.getColumnIndex(DB.COLUMN_URL)).equals(url))return cursor.getPosition();
    	}while(cursor.moveToNext());
    	return -1;
    }
    
}
