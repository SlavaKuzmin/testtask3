package com.example.testtask3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DB {

	final String LOG_TAG = "myLogs";
	public static final String DB_NAME = "mydb";
	public static final String DB_TABLE = "schtab";
	public static final String DB_TABLE_IMG = "imgtab";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_URL = "url";
	public static final String COLUMN_SEARCH = "search";
	private static final String COLUMN_LOCATION = "location";
	public static final String COLUMN_SURL = "surl";
	public static final String COLUMN_IMG = "gir_img";
	public static final String FILE_DIR = "dir_of_file";
	private static final int DB_VERSION = 1;
	
	
	private static final String DB_CREATE = 
			"create table " + DB_TABLE + "(" + 
				COLUMN_ID + " integer primary key autoincrement, " + 
				COLUMN_SEARCH + " text, " + 
				FILE_DIR + " text" + ");";
	
	
	private final Context mCtx;
	
	private DBHelper mDBHelper;
	private SQLiteDatabase mDB;
	
	public DB(Context ctx){
		mCtx=ctx;
	}
	
	public void open() {
	    mDBHelper = new DBHelper(mCtx, DB_NAME, null, DB_VERSION);
	   try{
		   mDB = mDBHelper.getWritableDatabase();
	   }catch (SQLiteException e){}
	}
	
	public void close() {
		if (mDBHelper!=null) mDBHelper.close();
	}	
	
	public void addSch(int id,String sch) {
	    ContentValues cv = new ContentValues();
	    cv.put(COLUMN_SEARCH, sch);
	    cv.put(COLUMN_ID, id);
	    mDB.insert(DB_TABLE, null, cv);
	}

	public void addUrl(int i ,String url,String surl) {
	    ContentValues cv = new ContentValues();
	    cv.put(COLUMN_URL, url);
	    cv.put(COLUMN_SURL, surl);
	    mDB.insert(DB_TABLE_IMG + Integer.toString(i), null, cv);
	}
	
	public void addDir(int INDEX ,String URL,String dir) {
	    ContentValues cv = new ContentValues();
	    cv.put(COLUMN_IMG, dir);
	    mDB.update(DB_TABLE_IMG + Integer.toString(INDEX), cv, COLUMN_URL+"=?", new String[] {URL});
	}
	
	public void addTab(int i) {
		mDB.execSQL("create table " + DB_TABLE_IMG + Integer.toString(i) + "(" + 
				COLUMN_ID + " integer primary key autoincrement, " +
				COLUMN_URL + " text, " + 
				COLUMN_SURL + " text," + 
				COLUMN_IMG + " text," +
				COLUMN_LOCATION + " text" + ");");
	}
	 
	public Cursor getAllData(String TabName) {
	    return mDB.query(TabName, null, null, null, null, null, null);
	}
	
	private class DBHelper extends SQLiteOpenHelper{
		
		public DBHelper(Context context, String name,
			CursorFactory factory, int version){
			super(context, name, factory, version);
		}
		
		public void onCreate(SQLiteDatabase db){
			db.execSQL(DB_CREATE);
		}
		
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
			
		}
	}
	
}
