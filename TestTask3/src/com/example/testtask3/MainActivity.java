package com.example.testtask3;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class MainActivity extends Activity {

	private Button btn;
	private AutoCompleteTextView txtSearch;
	private GridView gridImages;
	private ListViewImageAdapter adapter;
	private Activity activity;
	public static DB db;
	public static Cursor searches;
	public static String strSearch1 = "";
	public static String strSearch2 = "";
	final String LOG_TAG = "myLogs";
	String accountKeyEnc;
	public static ProgressBar progbar;
	public static int firstVisibleItem;
	public static int visibleItemCount;
	public static int totalItemCount;
	private static String[] requests=new String[]{""};
	ArrayAdapter<String> adapt;
	/** Called when the activity is first created. */
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		activity = this;
	    
		gridImages = (GridView)findViewById(R.id.gridImgID);
		btn= (Button)findViewById(R.id.searchButID);

		
	    db = new DB(this);
	    db.open();
	    searches = db.getAllData(DB.DB_TABLE);
	    //startManagingCursor(searches);
	    if(searches.getCount()!=0) updateResquests();
	    
		adapt = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, requests);
        txtSearch = (AutoCompleteTextView)
                findViewById(R.id.searchTxtID);
        txtSearch.setAdapter(adapt);
        txtSearch.setThreshold(0);
        txtSearch.setOnItemClickListener(new OnItemClickListener(){
        	public void onItemClick(AdapterView<?> parent, View view, int position, long id){
        		btn.performClick();
        	}
        });
        /*txtSearch.setOnFocusChangeListener(new OnFocusChangeListener() {          

            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)txtSearch.showDropDown();
                   //do job here owhen Edittext lose focus 
            }
        });*/
        txtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	if(!(requests[0].equals("")))
            	txtSearch.showDropDown();
            }

        });

	    Log.d("Picture", "f5");
	    gridImages.setOnItemClickListener(myGridViewClicked);
	    txtSearch.setOnEditorActionListener(onclSearch);
	    gridImages.setOnScrollListener(myOnScrollListener);
	    
        
	    /*adapter = (ListViewImageAdapter) getLastNonConfigurationInstance();
	    if(!(adapter==null)){
	    	adapter.link(this);
	    	gridImages.setAdapter(adapter);
	    }*/
	    
		String accountKey = "fJWgjlukTWW01UvY0sfk4J4hIRL+2wAkzDObqM221uQ";
		 byte[] accountKeyBytes = Base64.encodeBase64((accountKey + ":" + accountKey).getBytes());
        accountKeyEnc = new String(accountKeyBytes);

	    Log.d("Picture", "f1");
        //txtSearch.performClick();
	    Log.d("Picture", "f2");
        
	}
	
    public void updateResquests(){
    	searches.moveToFirst();
    	requests=new String[searches.getCount()];
	    do{
	    	requests[searches.getPosition()]=searches.getString(searches.getColumnIndex(DB.COLUMN_SEARCH));
	    }while(searches.moveToNext());
    }
    
    
	public void btnSearchClick(View v)
	   {
		strSearch2="";
	    strSearch1=txtSearch.getText().toString();
		if(strSearch1.equals("")) return;
		
		StringBuilder sb = new StringBuilder(strSearch1);
		StringBuilder sb1 = new StringBuilder();
		for (int index = 0; index < sb.length(); index++) {
		    char c = sb.charAt(index);
		    if (Character.isUpperCase(c)) {
		        sb.setCharAt(index, Character.toLowerCase(c));
		    } 
		    if(c==' '&&!(index==0)&&!(index==(sb.length()-1))) sb1.append("%20");
	        else sb1.append(Character.toLowerCase(c));
		}
		strSearch2=sb1.toString();
		 Log.d("Picture", strSearch2);
		strSearch1=sb.toString();
		searches.moveToFirst();

		InputMethodManager imm = (InputMethodManager)getSystemService(
			      Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(txtSearch.getWindowToken(), 0);
		if(searches.getCount()!=0) do{
				if(strSearch1.equals(searches.getString(searches.getColumnIndex(DB.COLUMN_SEARCH)))) {
					SetListViewAdapter(searches.getPosition());
					return;
				}
			}while(searches.moveToNext());
		if(!isNetworkConnected()){
			Toast.makeText(activity.getApplicationContext(), "����������� � ��������� �����������", Toast.LENGTH_SHORT).show();
			return;
		}

		   db.addSch(searches.getCount(),strSearch1);
		   db.addTab(searches.getCount());
		   searches = db.getAllData(DB.DB_TABLE);
		   updateResquests();
		   adapt = new ArrayAdapter<String>(this,
	                android.R.layout.simple_dropdown_item_1line, requests);
		   txtSearch.setAdapter(adapt);
		   new getImagesTask().execute();
	   }
	   
	OnEditorActionListener onclSearch = new OnEditorActionListener() {
	    @Override
	    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
	    	boolean handled = false;
	        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
	        	btn.performClick();
	            handled = true;
	        }
	        return handled;
	    }
	};
	
	
	  void logCursor(Cursor c) {
		    if (c != null) {
		      if (c.moveToFirst()) {
		        String str;
		        do {
		          str = "";
		          for (String cn : c.getColumnNames()) {
		            str = str.concat(cn + " = " + c.getString(c.getColumnIndex(cn)) + "; ");
		          }
		          Log.d(LOG_TAG, str);
		        } while (c.moveToNext());
		      }
		    } else
		      Log.d(LOG_TAG, "Cursor is null");
		  }

	  
	 public class getImagesTask extends AsyncTask<Void, Void, Void>
	   {
		 JSONObject json = new JSONObject();
		 protected void onPreExecute() {
			   super.onPreExecute();
		   }
		 
		 protected Void doInBackground(Void... params) {
			 
			   URL url;
			   try {
				   url = new URL("https://api.datamarket.azure.com/Bing/Search/v1/Composite?Sources=%27image%27&Query=%27"+URLEncoder.encode(strSearch1, "UTF-8")+"%27&ImageFilters=%27Size%3ALarge%27&VideoSortBy=%27Relevance%27&$format=json&$top=30&$skip="+0);
			   
			   URLConnection connection = url.openConnection();
			   connection.addRequestProperty("Authorization", "Basic " + accountKeyEnc);
			   String line;
			   StringBuilder builder = new StringBuilder();
			   BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			   while((line = reader.readLine()) != null) {
				   builder.append(line);
			   }

			   System.out.println("Builder string => "+builder.toString());
			   json=new JSONObject(builder.toString());
			   } catch (MalformedURLException e) {
				   // TODO Auto-generated catch block
				   e.printStackTrace();
			   } catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			   return null;
		   }
		 
		 protected void onPostExecute(Void result) {
			   // TODO Auto-generated method stub
			   super.onPostExecute(result);
			   try {
				   JSONObject responseObject = json.getJSONObject("d").getJSONArray("results").getJSONObject(0);
				   JSONArray resultArray = responseObject.getJSONArray("Image");
				   getImageList(resultArray);
			   } catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		   

			   SetListViewAdapter(searches.getCount()-1);
		   }
		 
	   }
	
	  public void getImageList(JSONArray resultArray)
	   {
		 	try 
			{
				for(int i=0; i<resultArray.length(); i++)
				{
					JSONObject obj;
					obj = resultArray.getJSONObject(i);
					db.addUrl(searches.getCount()-1,obj.getString("MediaUrl"),obj.getJSONObject("Thumbnail").getString("MediaUrl"));								   
				} 
			 }
			 catch (JSONException e) 
			 {
					// TODO Auto-generated catch block
					e.printStackTrace();
			 }
	   }
	  
	   public void SetListViewAdapter(int j)
	   {
		   adapter = new ListViewImageAdapter(activity, j);
		   gridImages.setAdapter(adapter);
		  }
	
	  public boolean onCreateOptionsMenu(Menu menu) {
	      // TODO Auto-generated method stub
	      // ��������� ������ ����
		  menu.add("������� �������");
	      
	      return super.onCreateOptionsMenu(menu);
	    }	
	
	  public boolean onOptionsItemSelected(MenuItem item) {
		  //stopManagingCursor(searches);
		  if(!(gridImages.getCount()==0))
			  ListViewImageAdapter.imageLoader.stopThread();
		  gridImages.setAdapter(null);
		  deleteDirectory(new File(android.os.Environment.getExternalStorageDirectory(),activity.getApplicationContext().getString(R.string.cache_dirname)));
		  
		  db.close();
		  activity.getApplicationContext().deleteDatabase(DB.DB_NAME);
		  db = new DB(activity.getApplicationContext());
		  db.open();
		  searches = db.getAllData(DB.DB_TABLE);
		  requests=new String[]{""};
		  txtSearch.setAdapter(null);
		  Toast.makeText(activity.getApplicationContext(), "������ ���� �������", Toast.LENGTH_SHORT).show();
		 // startManagingCursor(searches);
	      return super.onOptionsItemSelected(item);
	    }
	
	  
	  
	  OnItemClickListener myGridViewClicked = new OnItemClickListener() {

          @Override
          public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
              Intent intent = new Intent(MainActivity.this, Picture.class);
              intent.putExtra("Img", position);
              startActivity(intent); 
              
          }
      };
	  
     public static OnScrollListener myOnScrollListener = new OnScrollListener() {
          public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

          public void onScroll(AbsListView view, int firstVisibleItem,
                int visibleItemCount, int totalItemCount) {
        	  
        	  MainActivity.firstVisibleItem=firstVisibleItem;
        	  MainActivity.visibleItemCount=visibleItemCount;
        	  MainActivity.totalItemCount=totalItemCount;
            }
          };
      
      
      public static boolean deleteDirectory(File path) {
    	    if( path.exists() ) {
    	      File[] files = path.listFiles();
    	      if (files == null) {
    	          return true;
    	      }
    	      for(int i=0; i<files.length; i++) {
    	         if(files[i].isDirectory()) {
    	           deleteDirectory(files[i]);
    	         }
    	         else {
    	           files[i].delete();
    	         }
    	      }
    	    }
    	    return( path.delete() );
    	  }
      
      private boolean isNetworkConnected() {
    	  ConnectivityManager cm =
    		        (ConnectivityManager)activity.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
    		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    		boolean isConnected = activeNetwork != null &&
    		                      activeNetwork.isConnectedOrConnecting();
    		return isConnected;
    	 }
      
      /*protected void onSaveInstanceState(Bundle outState) {
    	    super.onSaveInstanceState(outState);
    	    outState.pu;
    	    Log.d(LOG_TAG, "onSaveInstanceState");
    	  }
      
      public Object onRetainNonConfigurationInstance() {
    	    // ������� �� MyTask ������ �� ������ MainActivity
    	  adapter.unLink();
    	    return adapter;
    	  }*/
      
      @Override
      public void onConfigurationChanged(Configuration newConfig) {
          super.onConfigurationChanged(newConfig);

          if (getResources().getConfiguration().orientation ==
                  Configuration.ORIENTATION_LANDSCAPE){
          	gridImages.setNumColumns(5);
          }
          else
          	gridImages.setNumColumns(3);
      }
      
	  protected void onDestroy() {
		    super.onDestroy();
		    if(!(gridImages.getCount()==0))
				  ListViewImageAdapter.imageLoader.stopThread();
		    db.close();
		  }
	  
}