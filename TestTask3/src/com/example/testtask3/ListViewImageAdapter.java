package com.example.testtask3;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


public class ListViewImageAdapter extends BaseAdapter {
    
	private Activity activity;
	public static int Index_of_search;
	private static LayoutInflater inflater=null;
	public static ImageLoader imageLoader; 
	public static Cursor resOfPrevSch;
	final String LOG_TAG = "myLogs";
	
    void link(MainActivity act) {
      activity = act;
    }

    void unLink() {
      activity = null;
    }
    
    public ListViewImageAdapter(Activity a, int Index_of_search) {
        activity = a;
        this.Index_of_search = Index_of_search;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        resOfPrevSch=MainActivity.db.getAllData(DB.DB_TABLE_IMG+Integer.toString(Index_of_search));
        imageLoader=new ImageLoader(activity.getApplicationContext(),Index_of_search,resOfPrevSch);
    }

    public int getCount() {
        return resOfPrevSch.getCount();
    }

    public Object getItem(int position) {
        //return position;
    	return position;
    }

    public long getItemId(int position) 
    {
    	return position;
    }
    
    public static class ViewHolder{
    	public ImageView imgViewImage;
    	public TextView txtViewTitle;
    	
    }

    void logCursor(Cursor c) {
	    if (c != null) {
	      if (c.moveToFirst()) {
	        String str;
	        do {
	          str = "";
	          for (String cn : c.getColumnNames()) {
	            str = str.concat(cn + " = " + c.getString(c.getColumnIndex(cn)) + "; ");
	          }
	          Log.d(LOG_TAG, str);
	        } while (c.moveToNext());
	      }
	    } else
	      Log.d(LOG_TAG, "Cursor is null");
	  }   
    
    
    public View getView(int position, View convertView, ViewGroup parent) {
    	View vi=convertView;
		if(convertView==null) vi = inflater.inflate(R.layout.item, null);
		imageLoader.DisplayImage(position, activity,(ImageView)vi.findViewById(R.id.picture),(ProgressBar)vi.findViewById(R.id.progressBar1));
		return vi;
    }
    
}